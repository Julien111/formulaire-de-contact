<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
        crossorigin="anonymous" />
    <link rel='stylesheet' href="css/style.css"/>
    <title>Formulaire de contact</title>
</head>

<body>


<div class="partForm">

                    <div class="formulaire">

                        <h1 id='contact'><i class="far fa-envelope"></i> Contact</h1>

                        <form action="formulaire.php" method="post" role="form" id="contactForm">

                            <div class="blocFrom">
                                <div class="nom">
                                    <p><label for="nom" class="h4">Nom</label></p>
                                    <input type="text" name="nom" class="form-control" id="name" placeholder="Nom"
                                        minlength="3" required>
                                </div>
                                <div class="prenom">
                                    <p><label for="prenom" class="h4">Prénom</label></p>
                                    <input type="text" name="prenom" class="form-control" id="prenom" placeholder="prenom"
                                        minlength="3" required>
                                </div>
                                <div class="email">
                                    <p><label for="email" class="h4">Email</label></p>
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Email"
                                        pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
                                </div>

                                <div class="objet">
                                    <p><label for="objet" class="h4">Objet</label></p>
                                    <input type="text" name="objet" class="form-control" id="objet" placeholder="Objet"
                                        minlength="3" required>
                                </div>

                                <div class="mess">
                                    <p><label for="message" class="h4 ">Message</label></p>
                                    <textarea name="message" id="message" class="form-control" rows="5"
                                        placeholder="Message" required></textarea>
                                </div>


                            </div>

                            <div class="bouton">
                                <button type="submit" id="form-submit" class="btn btn-sm pull-right">Envoyer</button>
                            </div>
                        </form>
                    </div>

                    <a href='admin.php'>Traitement des données</a>


                  
</body>

</html>