<?php


if (!empty($_POST)) { 

if(isset( $_POST["nom"]) && isset( $_POST["prenom"]) && isset( $_POST["email"]) && isset( $_POST["message"]) && isset( $_POST["objet"])){
 
    
    if(!empty( $_POST["nom"]) && !empty( $_POST["prenom"]) && !empty( $_POST["email"]) && !empty( $_POST["message"]) && !empty( $_POST["objet"])){
 
        
        if( filter_var($_POST["email"], FILTER_SANITIZE_EMAIL)){
 
            $_POST["email"] = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
 
            if( filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS) && filter_var($_POST["objet"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)){
                $_POST["message"] = filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                $_POST["objet"] = filter_var($_POST["objet"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
 
                if(filter_var($_POST["nom"], FILTER_SANITIZE_STRING) && filter_var($_POST["prenom"], FILTER_SANITIZE_STRING)) {
                    
                    $_POST["nom"] = filter_var($_POST["nom"], FILTER_SANITIZE_STRING);
                    $_POST['prenom'] = filter_var($_POST["prenom"], FILTER_SANITIZE_STRING);
                                    
                    printf('Bonjour, votre message a bien été envoyé.<br><a href="index.php">Retour à la page d\'accueil</a>');
                    
                    //Envoit du mail dans la BDD

                    //les variables

                    $nom = $_POST['nom'];
                    $prenom = $_POST['prenom'];
                    $email = $_POST['email'];
                    $objet = $_POST['objet'];
                    $message = $_POST['message'];

                    //Connection à la BDD

                    try {
                        require("requete/bdd.php");

                        //Fonction pour se connecter t_email et récup un email                        
                        //Envoyer les données en BDD

                        if (! ($MyDB->inTransaction())) {
                            try {
                        
                                    $MyDB->beginTransaction();

                                    //D'abord l'email

                                    //on test si l'email est présent en BDD

                                    $emailBDD = $MyDB->prepare("SELECT id_Email, Email FROM t_email WHERE Email = :valEmail");
                                    $emailBDD->bindParam(':valEmail', $email);
                                    $emailBDD->execute();
                                    $infos = $emailBDD->fetchAll(PDO::FETCH_OBJ);

                                    //Si le tableau est vide insère dans la table t_email : l'email

                                    if(empty($infos)){

                                        //On insère l'email

                                        require_once("requete/requeteSql.php");
                                        
                                        $requete = $MyDB->prepare(REQ_INSERT_MAIL);
                                        $requete->bindParam(':valEmail', $email);
                                        $requete->execute();

                                        //Ensuite récup id_email

                                        $recup_IdEmail = $MyDB->prepare(RECUP_ID_EMAIL);
                                        $recup_IdEmail->bindParam(':valueEmail', $email);
                                        $recup_IdEmail->execute();
                                        $datas = $recup_IdEmail->fetchAll(PDO::FETCH_OBJ);

                                        foreach($datas as $mail):

                                            $idMail = $mail->id_Email;

                                            //On envoit les données dans t_personne et t_msg

                                            require_once("requete/requeteSql.php");

                                            $insertInfo = $MyDB->prepare(REQ_INSERT_PERSONNE);
                                            $insertInfo->bindParam(':prenom', $prenom);
                                            $insertInfo->bindParam(':nom', $nom);
                                            $insertInfo->bindParam(':id_email', $idMail);
                                            $insertInfo->execute();

                                            //On insère dans t_personne

                                            $insertmsg = $MyDB->prepare(REQ_INSERT_MSG);
                                            $insertmsg->bindParam(':objet', $objet);
                                            $insertmsg->bindParam(':msg', $message);
                                            $insertmsg->bindParam(':id_email', $idMail);
                                            $insertmsg->execute();

                                        endforeach;   

                                    }
                                    else{
                                        foreach($infos as $mail):
                                        
                                        //var_dump($mail->Email);
                                        
                                        //Si l'email est déjà présent
                                        //Si le tableau est plein, on récupère l'id de l'email

                                        if($mail->Email === $email){

                                            $Id_mail = $mail->id_Email;
                                            echo "le mail existe";

                                            require_once("requete/requeteSql.php");

                                            //On insère les datas dans t_personne

                                            $insertInfo = $MyDB->prepare(REQ_INSERT_PERSON);
                                            $insertInfo->bindParam(':prenom', $prenom);
                                            $insertInfo->bindParam(':nom', $nom);
                                            $insertInfo->bindParam(':id_email', $Id_mail);
                                            $insertInfo->execute();

                                            //On insère les datas dans t_msg
                                            
                                            $insertMsg = $MyDB->prepare(REQ_INSERT_MESSAGE);
                                            $insertMsg->bindParam(':objet', $objet);
                                            $insertMsg->bindParam(':msg', $message);
                                            $insertMsg->bindParam(':id_email', $Id_mail);
                                            $insertMsg->execute();  
                                            
                                        }                                   
                                        endforeach;

                                        
                                    }    

                                    


                                    $MyDB->commit();
                        
                            } catch (Exception $ExceptionRaised) {
                                    $MyDB->rollBack();
                                    print_r($MyDB->errorInfo());
                                    print_r($MyRequest->errorInfo());
                                    die("Error: " . $ExceptionRaised->getMessage());
                            }
                        }else{
                         print 'Impossible de commencer la transaction. Une autre transaction est déjà en cours.';
                        }

                    } catch (Exception $ExceptionRaised) {
                        printf("Erreur dans l'envoit du message. Impossible de traîter les données");

                    }

                    //Fin
                   
                    $to = "jj@gmail.com";
                    $subject = "Vous avez un nouveau contact";
                    $message = $_POST["nom"].' vous a écrit :<br>'. $_POST["email"]."<br>Contenu du message : <br>" . $_POST["message"]."<br>";
                   
                    $headers = 'From: julien.farre@labo-ve.fr '. PHP_EOL .
                    'Reply-To: ' .$_POST["email"]. PHP_EOL .
                    'Content-Type: text/html; charset=UTF-8'. PHP_EOL .
                    'X-Mailer: PHP/' . phpversion() ;
                    mail($to, $subject, $message, $headers);

                    //printf("Message envoyé");

                
                }else {$alerte = '<a href="index.php">Il semble que les données que vous avez saisies ne soient pas valides</a>';
                }
 
            }else {$alerte = '<a href="index.php">Votre message contient sans doute des caractéres non appropriés</a>';
            }
 
        }else {$alerte = '<a href="index.html">Merci de saisir une adresse email valide</a>';
        }
 
    }else {$alerte = '<a href="index.html">Merci de remplir les champs obligatoires</a>';
    }
 
}else {$alerte = '<a href="index.html">ok</a>';
}
 
}
?>
<?php if (isset($alerte)) { echo $alerte; } ?>


