<?php

//Traitement des données du formulaire de contact

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href="css/index.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Traitement des informations </title>
</head>

<body>

<div class='container'>
<div class='titrePrincipal'>
    <h1>Page de traitement des messages du formulaire de contact</h1>
</div>

    <h3>Afficher tous les messages avec le statut "A traiter ou "A relancer"</h3>

    <form action="admin.php" method='post'>
        <label for='affiche'>Les messages à relancer ou à traîter: </label>
        <input type='submit' value='Afficher' name='traitement' id='traitement' />
    </form>

<?php

    //On affiche les messages dans un tableau

if(isset($_POST['traitement']) && !(empty($_POST['traitement']))):

?>


    <div class='traitement'>
        

        <table class="table-striped">
            <tr>
                <th>ID</th>
                <th>Objet</th>
                <th>Message</th>
                <th>Date</th>
                <th>Etat</th>
                <th>Nom</th>                
                <th>Changer l'état</th>                
            </tr>

        <?php  
            require_once("requete/requeteSql.php");

            $trait = getMessageATraiter();

            while ($traitement = $trait->fetch(PDO::FETCH_NUM)){

            //var_dump($msgs);   
            
            printf("<tr>");

                foreach($traitement as $elt){
                    
                    printf('<td>'.filter_var($elt, FILTER_SANITIZE_STRING).'</td>');

                }
                    
         printf('<td>');           
        ?>

           
            <form action="admin.php" method='post'>

                <p><label for='affiche'>Confirmer l'id du messages : </label>

                <input id='test' type='text' pattern="[0-9]{1,}" maxlength="3" name='id' required/></p>        
      
                <label for="pet-select">Changer l'état :</label>

            <select name="etat" id="etat">
                <option value="Sans suite">Sans suite</option>
                <option value="A traiter">A traiter</option>
                <option value="En attente">En attente</option>
                <option value="A relencer">A relancer</option>
                <option value="Rendez-vous pris">Rendez-vous pris</option>
            
            </select>

            <input type='submit' value='Modifier' name='modifier' id='modif' />

            </form>

           

        <?php

                     printf("</td>");
                    
                

            printf("</tr>");        
                

            

            

        }
            

        ?>

        </table>
    
    </div>


     <form action='admin.php' method='post' class='btn bg-danger'>
        <input type="submit" value='Cacher le tableau' class='btn'/>
    </form>

<?php

    
else:
    echo '';
endif;
?>


    <h3>Afficher tous les messages</h3>

    <form action="admin.php" method='post' class='btn'>
        <label for='affiche'>Afficher tous les messages</label>
        <input type='submit' value='Afficher' name='affiche' id='affiche' />
    </form>

    

<?php

    //On affiche tous les messages dans un tableau

if(isset($_POST['affiche']) && !(empty($_POST['affiche']))):

?>
    <div class='affiche'>
        

        <table class="table-striped">
            <tr>
                <th>ID</th>
                <th>Objet</th>
                <th>Message</th>
                <th>Date</th>
                <th>Email</th>                
                <th>Nom</th>
                <th>Prenom</th>
                
            </tr>
            

<?php  
            require_once("requete/requeteSql.php");

            $messages = getMessage();

            while ($msgs = $messages->fetch(PDO::FETCH_NUM)){

            //var_dump($msgs);   
            
            printf("<tr>");

                foreach($msgs as $ligne){
                    
                    printf('<td>'.filter_var($ligne, FILTER_SANITIZE_STRING).'</td>');
                    
                }

            printf("</tr>");

            }       
            

            ?>

        </table>

    </div>
    
    <form action='admin.php' method='post' class='btn bg-danger'>
        <input type="submit" value='Cacher le tableau' class='btn'/>
    </form>

    <?php

    
else:
    echo '';
endif;

//Fin de l'affichage des messages dans un tableau


//Afficher les message par mois

?>

<h3>Les messages reçus par mois</h3>

<!-- On récupère le numéro du mois -->


<form action="admin.php" method='post'>
        <p><label for='affiche'>Afficher tous les messages par mois : </label></p>
        <label>Tapez le numéro du mois exemple : <strong> 01 (janvier), 02 (février) etc</strong>.</label>
        <input type='text' maxlength=2 pattern="[0-9]{2}" name='month' id='month'/>
        <input type='submit' value='Résultat' name='mois' id='mois' />

        <p><strong id='color'>(*) Si aucun message ne s'affiche, le mois choisi ne contient aucun messages.</strong></p>
</form>

<div id=mois>

<?php

//On test si on reçoit bien les données du formulaire

if(isset($_POST['mois']) && !(empty($_POST['mois'])) && !empty($_POST['month'])):

    require_once("requete/requeteSql.php");

    $numMois = $_POST['month'];

    $messMois = getMessageMonth($numMois);

    //var_dump($messMois);

   
    //Si il y a pas de données alors on affiche rien sinon exécute la requête

    if(empty($messMois)):

        printf("Aucune données à afficher pour le mois en cours.");
    
    else:
?>

<table class="table-striped">
            <tr>
                <th>ID</th>
                <th>Objet</th>
                <th>msg</th>
                <th>etat</th>               
                <th>Prenom</th>
                <th>Nom</th>
            </tr>
<?php

     while ($msgMonth = $messMois->fetch(PDO::FETCH_NUM)){

            //var_dump($msgs);   
            
            printf("<tr>");

                foreach($msgMonth as $element){
                    
                    printf('<td>'.filter_var($element, FILTER_SANITIZE_STRING).'</td>');
                    
                }

            printf("</tr>");

    }

?>

</table>

 <form action='admin.php' method='post' class='btn bg-danger'>
        <input type="submit" value='Cacher le tableau' class='btn'/>
 </form>

</div>

<?php

    endif;



else:
    printf("");
endif;

//Fin de la partie afficher les messages par mois

?>

<div class='etat'>

<h3>Modifier le statut d'un messages</h3>

<!-- On récupère le numéro du mois -->


<form action="admin.php" method='post'>

        <p><label for='affiche'>Rentrer l'id du messages : </label></p>

        <input type='text' pattern="[0-9]{1,}" maxlength="3" name='id' id='id' required/>
        <div>
            <input type="radio" id="suite" name="etat" value='Sans suite'
                checked>
            <label for="suite">Sans suite</label>
        </div>

        <div>
            <input type="radio" id="relancer" name="etat" value='A relancer'>
            <label for="relancer">A relancer</label>
        </div>

        <div>
            <input type="radio" id="traiter" name="etat" value='A traiter'>
            <label for="traiter">A traiter</label>
        </div>

        <div>
            <input type="radio" id="attente" name="etat" value='En attente'>
            <label for="attente">En attente</label>
        </div>

        <div>
            <input type="radio" id="attente" name="etat" value='Rendez-vous pris'>
            <label for="rdv">Rendez-vous pris</label>
        </div>

        <input type='submit' value='Modifier' name='modifier' id='modif' />

        
</form>

<?php

//var_dump($_POST['etat']);
//var_dump($_POST['id']);

if(!(empty($_POST['etat'])) && !(empty($_POST['id']))):

    require_once("requete/requeteSql.php");

    

    $etat = filter_var($_POST['etat'], FILTER_SANITIZE_STRING);
    $id = filter_var($_POST['id'], FILTER_SANITIZE_STRING);

    $ID = intval($id);

    if($ID){

        changeEtat($ID, $etat);

        printf("L'état a bien été modifié");
    }
    else{
        printf('Erreur dans le traitement');
    }

else: 
    printf("");

endif;
?>
</div>


<div class='perso'>

    <h3>Afficher les message d'une personne en particulier</h3>

    <form action="admin.php" method='post'>
        <p><label for='email'>Rentrer l'email de la personne : </label></p>

            <input type="email" name="email" id="email" placeholder="Email"
                pattern="[A-Za-z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,4}$" required>        

         <input type='submit' value="Envoyer" name='envoyer'/>
    
    </form>
 </div>

    <?php

//var_dump($_POST['envoyer']);
//var_dump($_POST['email']);   Bien mettre des names différents dans les submits car sinon == NULL

if(isset($_POST['email']) && !(empty($_POST['email']))):

    require_once("requete/requeteSql.php");

    

    $mail = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);

    //var_dump($mail);
   
    
    if($mail){

        $result = getMessagePerso($mail);

    ?>

    <table class="table-striped">
            <tr>
                <th>ID message</th>
                <th>Objet</th>
                <th>Message</th>
                <th>Date</th>                
                <th>Email</th>                
                <th>Prenom</th>
                <th>Nom</th>
            </tr>
<?php

     while ($data = $result->fetch(PDO::FETCH_NUM)){

           
            
            printf("<tr>");

                foreach($data as $element){
                    
                    printf('<td>'.$element.'</td>');
                    
                }

            printf("</tr>");

    }

?>

    </table>

     <form action='admin.php' method='post' class='btn bg-danger'>
            <input type="submit" value='Cacher le tableau' class='btn'/>
     </form>

   



    <?php            
    }
    else{
        printf('Erreur dans le traitement');
    }

else: 
    printf("");

endif;
?>

</div>


</body>

</html>