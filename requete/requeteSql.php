<?php


//Insérer les données dans les trois tables

//Commencer par l'email si l'email n'existe pas

const REQ_INSERT_MAIL = "INSERT INTO t_email (Email) values (:valEmail)";

const RECUP_ID_EMAIL = "SELECT id_Email FROM t_email WHERE Email = :valueEmail";

const REQ_INSERT_PERSONNE = "INSERT INTO t_personne (prenom, nom, id_email_personne) VALUES (:prenom, :nom, :id_email)";

const REQ_INSERT_MSG = "INSERT INTO t_msg(objet, msg, id_email) VALUES (:objet, :msg, :id_email)";

//requêtes à faire si l'email existe

const REQ_INSERT_PERSON = "INSERT INTO t_personne (prenom, nom, id_email_personne) values (:prenom, :nom, :id_email)";

const REQ_INSERT_MESSAGE = "INSERT INTO t_msg (objet, msg, id_email) values (:objet, :msg, :id_email)";


//Fin

//##  Les requêtes pour admin.php (récupérer et analyser les données) ##//


//Afficher tous les messages avec une fonction getMessage()

const REQ_MESSAGES_DATE = "SELECT DISTINCT id_msg, objet, msg, date_msg, Email, prenom, nom FROM t_msg INNER JOIN t_email ON t_msg.id_email = t_email.id_Email INNER JOIN t_personne ON t_msg.id_email = t_personne.id_email_personne ORDER BY date_msg DESC";

function getMessage (){

try {

    require_once("bdd.php");

    $messageInfo = $MyDB->prepare(REQ_MESSAGES_DATE);
    $messageInfo->execute();
    if($messageInfo->rowCount() > 0) //si il y a des données
    {
        return $messageInfo;
    }
    else{
        return "";
    }
    
$messageInfo->closeCursor();
    
} catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
}



}

//Afficher les messages par mois via la fonction getMessageMonth

const REQ_MESSAGE_MOIS = "SELECT DISTINCT id_msg, objet, msg, etat, prenom, nom FROM t_msg JOIN t_personne ON id_email = id_email_personne WHERE month(date(date_msg)) = :Months";

function getMessageMonth ($number){

    $n = filter_var($number, FILTER_SANITIZE_STRING);

    $num = htmlspecialchars($n);

try {

    if('01' === $num || '02' === $num || '03' === $num || '04' === $num || '05' === $num ||$num === '10' || '11' === $num  || '12' === $num || '06' === $num ||'07'=== $num || '08' === $num || '09' === $num){

    //var_dump($num);

    require_once("bdd.php");
    
    $messageMonth = $MyDB->prepare(REQ_MESSAGE_MOIS);
    $messageMonth->bindParam(':Months', $num);
    $messageMonth->execute();

    if($messageMonth->rowCount() > 0) //si il y a une correspondance

    {
        return $messageMonth;
    }
    else{
        return "";


    }

    $messageMonth->closeCursor();

   
    }
    else{
        header('Location: admin.php');
    }
    
    
} catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
}



}

//Transaction pour modifier l' état d'un message
//Utilisation de la fonction change état;

const REQ_MODIF_ETAT = "UPDATE t_msg SET etat = :ValEtat WHERE id_msg = :id";

function changeEtat ($id, $etat){

    try {

        require_once("bdd.php");

      
           
            $ID = filter_var($id, FILTER_SANITIZE_STRING);
            $etat = filter_var($etat, FILTER_SANITIZE_STRING);

            if (! ($MyDB->inTransaction())) {
                try {
                
                    $MyDB->beginTransaction();

                        $insertEtat = $MyDB->prepare(REQ_MODIF_ETAT);
                        $insertEtat->bindParam(':ValEtat', $etat);
                        $insertEtat->bindParam(':id', $ID);
                        $insertEtat->execute();

                    $MyDB->commit();
                }
                catch (Exception $ExceptionRaised) {

                    $MyDB->rollBack();
                    print_r($MyDB->errorInfo());
                    print_r($MyRequest->errorInfo());
                    die("Error: " . $ExceptionRaised->getMessage());
                }

            }
    }
    
    catch (Exception $ExceptionRaised) {
        
        printf("Erreur, impossible de maudifier l'état");

    }

    
}

//Requête afficher les messages avec le statut A traiter et A relancer 

const REQ_AFFICHE_MESS_TRAIT = "SELECT DISTINCT id_msg, objet, msg, date_msg, etat, nom FROM t_msg JOIN t_personne ON id_email = id_email_personne WHERE etat = 'A relancer' OR etat = 'A traiter'";

function getMessageATraiter (){

try {

    require_once("bdd.php");

    $messageTrait = $MyDB->prepare(REQ_AFFICHE_MESS_TRAIT);
    $messageTrait->execute();
    if($messageTrait->rowCount() > 0) //si il y a des données
    {
        return $messageTrait;
    }
    else{
        return "";
    }
    
    $messageTrait->closeCursor();
    
} catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
}

}

//Afficher les messages d'une persone en particulier
//La fonction getMessagePerso() affiche tous les messages d'une personne selon son email 


const REQ_AFFICHE_MESS_PERSON = "SELECT DISTINCT id_msg, objet, msg, date_msg, Email, prenom, nom FROM t_msg INNER JOIN t_email ON t_msg.id_email = t_email.id_Email INNER JOIN t_personne ON t_msg.id_email = t_personne.id_email_personne WHERE Email = :email";

function getMessagePerso ($mail){

     try {

        require_once("bdd.php");

        if(empty($mail)){
            header("Location: admin.php");
        }
        if(filter_var($mail, FILTER_SANITIZE_EMAIL)){ 
            
            $email = filter_var($mail, FILTER_SANITIZE_EMAIL);

            $messagePerso = $MyDB->prepare(REQ_AFFICHE_MESS_PERSON);
            $messagePerso->bindParam(':email', $email);
            $messagePerso->execute();

            if($messagePerso->rowCount() > 0) //si il y a des données
            {
                return $messagePerso;
            }
            else{
                header("Location: admin.php");
            }    
            $messagePerso->closeCursor();
        }
        else{
            header("Location: admin.php");
        }

    } catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }   

}

//Fin des messages selon l'email


//selectionner l'etat

const REQ_MODIF_STATUT_ETAT = "SELECT etat FROM t_msg WHERE id_msg = :message_ID";

//afficher l'etat

const REQ_AFFICHE_ETAT = "SELECT etat FROM t_msg WHERE id_msg = :message_ID";

//Modifier l'etat d'un message partie 1 

const REQ_ETAT = "UPDATE t_msg SET etat = :result WHERE id_msg = :message_ID";

//Grouper les messages par id

const REQ_MESSAGES_GROUP = "SELECT * FROM t_msg,t_email WHERE t_msg.id_Email = t_email.id_email";

//sÃ©lectionner l'id et recup tous les messages d'une personne

const REQ_MESSAGES_PERSON = "SELECT msg FROM t_msg JOIN t_personne ON id_email = id_email_personne WHERE id_email_personne = 3";


//  use laboadm_jfarre;
// insert into t_email (Email) values (:valEmail);
// set @mon_email = (select id_Email from t_email where Email = :valEmail);
// insert into t_personne (prenom,nom,id_email_personne) values (:valPrenom,:valNom,@mon_email);
// insert into t_msg (sujet, msg, id_email) values (:valSujet,:valMsg,@mon_email);